#include <iostream>
#include <memory>

struct Camara {
    int CantidadDisparos;
    std::string marca;
    int FechaFabricacion;
};

bool VerificarLimiteDisparos(std::shared_ptr<Camara>& camara_ptr) {
    const int Disparos = 100;
    if (camara_ptr->CantidadDisparos >= Disparos) {
        std::cout << "La camara alcanzo el limite de disparos permitido." << std::endl;
        camara_ptr.reset();
        return true;
    }
    return false;
}

int main() {
    std::shared_ptr<Camara> camara_ptr(new Camara{0, "Canon", 2020});

    while (true) {
        if (VerificarLimiteDisparos(camara_ptr)) {
            std::cout << "La camara ha sido destruida." << std::endl;
            break;
        }
        camara_ptr->CantidadDisparos++;
    }

    std::weak_ptr<Camara> weak_camara_ptr(camara_ptr);
    camara_ptr.reset();
    if (weak_camara_ptr.expired()) {
        std::cout << "La camara ha sido destruida." << std::endl;
    } else {
        std::cout << "La camara aun existe." << std::endl;
    }

    return 0;
}