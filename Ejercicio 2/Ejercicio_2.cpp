#include <iostream>
#include <string>
#include <memory>

using namespace std;

// Struct para almacenar un recordatorio
struct Recordatorio {
    string titulo;
    string descripcion;
    string fecha;
    string hora;
};

// Struct para almacenar un nodo en la lista enlazada
struct Node {
    shared_ptr<Node> previo;
    shared_ptr<Node> siguiente;
    shared_ptr<Recordatorio> nota;
};

// Crear un nuevo nodo con un recordatorio
Node* crearNodo(Recordatorio* nota) {
    Node* nodo = new Node;
    nodo->previo = nullptr;
    nodo->siguiente = nullptr;
    nodo->nota = shared_ptr<Recordatorio>(nota);
    return nodo;
}

// Agregar un nuevo nodo al final de la lista
void agregarNodo(Node* &inicio, Recordatorio* nota) {
    Node* nodo = crearNodo(nota);
    if (inicio == nullptr) {
        inicio = nodo;
    } 
    else {
        Node* ultimo = inicio;
        while (ultimo->siguiente != nullptr) {
            ultimo = ultimo->siguiente.get();
        }
        nodo->previo = shared_ptr<Node>(ultimo);
        ultimo->siguiente = shared_ptr<Node>(nodo);
    }
}

// Mostrar todos los recordatorios en lista
void mostrarRecordatorios(Node* inicio) {
    Node* actual = inicio;
    while (actual != nullptr) {
        cout << "Titulo: " << actual->nota->titulo << endl;
        cout << "Descripcion: " << actual->nota->descripcion << endl;
        cout << "Fecha: " << actual->nota->fecha << endl;
        cout << "Hora: " << actual->nota->hora << endl;
        cout << endl;
        actual = actual->siguiente.get();
    }
}

// Buscar recordatorio por título
void buscarRecordatorio(Node* inicio, string titulo) {
    Node* actual = inicio;
    while (actual != nullptr) {
        if (actual->nota->titulo == titulo) {
            cout << "Titulo: " << actual->nota->titulo << endl;
            cout << "Descripcion: " << actual->nota->descripcion << endl;
            cout << "Fecha: " << actual->nota->fecha << endl;
            cout << "Hora: " << actual->nota->hora << endl;
            return;
        }
        actual = actual->siguiente.get();
    }
    cout << "No se encontró el recordatorio con título " << titulo << endl;
}

// Eliminar un recordatorio por título
void eliminarRecordatorio(Node* &inicio, string titulo) {
    Node* actual = inicio;
    while (actual != nullptr) {
        if (actual->nota->titulo == titulo) {
            if (actual->previo == nullptr) {
                inicio = actual->siguiente.get();
                if (inicio != nullptr) {
                    inicio->previo = nullptr;
                }
            } else {
                actual->previo->siguiente = actual->siguiente;
                if (actual->siguiente != nullptr) {
                    actual->siguiente->previo = actual->previo;
                }
            }
            delete actual;
            cout << "El recordatorio con título " << titulo << " ha sido eliminado." << endl;
            return;
        }
        actual = actual->siguiente.get();
    }
    cout << "No se encontró el recordatorio con título " << titulo << " para eliminar." << endl;
}

int main() {
Node* inicio = nullptr;
int opcion;
do {
cout << "Elija una de las siguientes opciones" << endl;
cout << "1.Nuevo recordatorio" << endl;
cout << "2.Mostrar todos los recordatorios" << endl;
cout << "3.Buscar recordatorio por título" << endl;
cout << "4.Eliminar recordatorio por título" << endl;
cout << "5.Salir" << endl;
cout << "Ingrese su opcion: ";
cin >> opcion;
cout << endl;

    if (opcion == 1) {
        // Pedir información de recordatorio
        Recordatorio* nota = new Recordatorio;
        cout << "Ingrese el titulo: ";
        cin >> nota->titulo;
        cout << "Ingrese la descripcion: ";
        cin >> nota->descripcion;
        cout << "Ingrese la fecha: ";
        cin >> nota->fecha;
        cout << "Ingrese la hora: ";
        cin >> nota->hora;
        cout << endl;
        agregarNodo(inicio, nota);
    } else if (opcion == 2) {
        mostrarRecordatorios(inicio);
    } else if (opcion == 3) {
        string titulo;
        cout << "Ingrese el titulo del recordatorio a buscar: ";
        cin >> titulo;
        cout << endl;
        buscarRecordatorio(inicio, titulo);
    } else if (opcion == 4) {
        string titulo;
        cout << "Ingrese el titulo del recordatorio a eliminar: ";
        cin >> titulo;
        cout << endl;
        eliminarRecordatorio(inicio, titulo);
    } else if (opcion == 5) {
        cout << "Hasta luego!" << endl;
    } else {
        cout << "Opcion invalida. Intente nuevamente." << endl;
    }
} while (opcion != 5);

// Liberar la memoria de los nodos y recordatorios en la lista
Node* actual = inicio;
while (actual != nullptr) {
    Node* siguiente = actual->siguiente.get();
    delete actual->nota.get();
    delete actual;
    actual = siguiente;
}
return 0;
}